package in3k8

import java.net.URI
import java.net.URISyntaxException

import org.java_websocket.client.WebSocketClient
import org.java_websocket.drafts.Draft
import org.java_websocket.drafts.Draft_10
import org.java_websocket.handshake.ServerHandshake


class SocketClient extends WebSocketClient {

	public SocketClient( URI serverUri , Draft draft ) {
		super( serverUri, draft )
	}

	public SocketClient( URI serverURI ) {
		super( serverURI )
	}

	@Override
	public void onOpen( ServerHandshake handshakedata ) {
		println( "opened connection" )
	}

	@Override
	public void onMessage( String message ) {
		//println( "received: " + message )
		// send( "you said: " + message )
	}

	@Override
	public void onClose( int code, String reason, boolean remote ) {
		println( "Connection closed by " + ( remote ? "remote peer" : "us" ) )
	}

	@Override
	public void onError( Exception ex ) {
		ex.printStackTrace()
		// if the error is fatal then onClose will be called additionally
	}


}