package in3k8

import java.net.URI
import org.java_websocket.drafts.*
class SendMessageController {
	def index() {
		SocketClient wsClient = new SocketClient(new URI('ws://localhost:8080/ws-grails/ws/informer'),new Draft_17())
		if (wsClient.connectBlocking()) {
			println 'sending message....'
			wsClient.send(' hi there hello')
			render(status:200,text:'{}',contentType:'application/json')
		} else {
			println 'not sending message.... no connection'
			render(status:404,contentType:'text/plain')			
		}
		
	}
}